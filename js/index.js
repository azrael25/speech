(function($) {
    window.$ = $;
    $(initApp);
})(jQuery);

function initApp() {
    initAudio();
    initEvents();
}

// Globals
var isRecording = false;

function initEvents() {
    $(document).on('keyup', function(event) {
        event.keyCode === 32 && toggleRecording();
    });
    $(window).on('click tap', function(event) {
        toggleRecording();
    });
}

function toggleRecording() {
    if (isRecording) {
        // stop recording
        $('.mic').removeClass('recording');
        audioRecorder.stop();
        // audioRecorder.getBuffers( gotBuffers );
        audioRecorder.getBuffers(function() {
            audioRecorder.exportWAV(upload);
        });
        isRecording = false;
    } else {
        // start recording
        $('#result').html('');
        $('#result').hide('');
        if (!audioRecorder) {
            return;
        }
        $('.mic').addClass('recording');
        audioRecorder.clear();
        audioRecorder.record();
        isRecording = true;
    }
}

function upload(blob) {
    var fd = new FormData();

    fd.append('sound', blob);

    $.ajax({
        type: 'POST',
        url: '/upload',
        data: fd,
        processData: false,
        contentType: false
    })
        .done(function(data) {
            console.log(data);
            $('#result').html(data);
            $('#result').fadeIn();
            setTimeout(function() {
                $('#result').fadeOut();
            }, 5000);
        });
}
